# ¿ Cúal es la mejor forma de recorrer un arreglo en js?
para comprobar la eficiencia de cada metodo planteado usaremos la biblioteca de avaluación conparativa benchmark para analizar el ops/sec de cada alternativa.  

- node v14.17.0
- ubuntu linux v20
- CPU intel Core i5
- 16 Gb RAM

Ejecutar en el terminal:

    $ node testiterate.js

obtendremos como respuesta:

    while x 3,168,497 ops/sec ±0.60% (92 runs sampled)
    for (let i) x 3,191,620 ops/sec ±0.53% (96 runs sampled)
    for (let of) x 577,287 ops/sec ±2.26% (91 runs sampled)
    for (let in) x 49,363 ops/sec ±1.49% (85 runs sampled)
    foreach x 2,367,041 ops/sec ±66.69% (97 runs sampled)
    _.forEach x 77,076 ops/sec ±0.62% (92 runs sampled)
    Fastest is for (let i)

Indicando que la forma más eficiente de recorrer un arreglo en js es el método tradicional:

    for (let i = 0; i < array.length; ++i) {
        ...
    }

# Semilla de datos 
La semilla de datos esta disponible desde el archivo loadData en el directorio lib, el mismo puede ser invocado desde el index de la aplicación, para ello es requerido  la variable de ambiente LOAD_DATA tenga el valor true la misma puede ser modificada desde el archivo config y cargada desde la configuracion del servicio en en yml de docker compose.

Ejemplo :

    environment:
        - LOAD_DATA=true

El funcionamiento del mismo puedo ser analizado desde los logs del sistema.


# API REST
Éste servicio de API permite visializar los registros de equipos y sus judares segun el requerimiento inicial, el mismo posee un proseceso basico de autenticación
## Despliegue del servico
### Requerimientos
- tener en ejecusion un servicio de mongodb (el mismo puede ser desplegado desde el arachivo doker-compose.yml del proyecto).

### Inicio
Para arrancar el servicio API se puede ejecutar desde la consola el comando:


    $ yarn run start

### Inicio desde Docker - docker-compose
Ejecutar:

    $ docker-compose up -d

Se levantaran todos los servicios, incluyendo el servicoi de mongodb.

Para ver todos los logs de una determinada imagen se puede ejecutar el comando.  

    $ docker-compose logs -f <continner name> 

### Usabilidad
Una vez desplegado el servicio de api tendremos los siguentes endpoinds disponibles:

POST /users - registrar un usuario con el siguiente contrato:

        {
            "email": "email@test.com",
            "padword": "paswordtest"
        }
Éste retornará el id del usuario registrado 

POST /auth - auntenticar un usuario mediante el siguiente contrato:

    {
        "email": "email@test.com",
        "password": "paswordtest"
    }

La respuesta de éste endpoind será el token de validación del usuario, ejemplo:

    {
        "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MGE3YjEyMmI3M2IwZjM5ZTgzMjNmYWIiLCJlbWFpbCI6InRlc3RAdGVzdC5jb20iLCJ1c2VyTGV2ZWwiOjE0NSwicHJvdmlkZXIiOiJlbWFpbCIsInJlZnJlc2hLZXkiOiJiNmhEZHN5aGVJanR5a0laT25WM0dnPT0iLCJpYXQiOjE2MjE2MDM4MTF9.bCuIKf-6kaauTAXSXAP1bVxKFgFDVEtp7r9KrMecTdA",
        "refreshToken": "VEE5QzUvS0dXY1dFbnM4a0Q1SGNCUWNTNUg0UG84U3BWWXl2MnlENktYNFdUcEZiR0JPM3BtSDZ3VndMeTJHS2hnVm1kamQxVGJLSE5jRmpUS1BYU2c9PQ=="
    }


POST /auth/refresh - actualizar token de usuario.

GET /users - listado de usuarios registrados. Se requiere la cabecera Authorization.

        Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MGE3YjEyMmI3M2IwZjM5ZTgzMjNmYWIiLCJlbWFpbCI6InRlc3RAdGVzdC5jb20iLCJ1c2VyTGV2ZWwiOjE0NSwicHJvdmlkZXIiOiJlbWFpbCIsInJlZnJlc2hLZXkiOiJiNmhEZHN5aGVJanR5a0laT25WM0dnPT0iLCJpYXQiOjE2MjE2MDM4MTF9.bCuIKf-6kaauTAXSXAP1bVxKFgFDVEtp7r9KrMecTdA

La respuesta de éste servico es:

    [
    {
        "_id": "60a7b122b73b0f39e8323fab",
        "email": "email@test.com",
        "password": "gjPlEHsXKbLpCuSrykPOwQ==$AleFYDQYf0F+frFP4W3sFcPCK5kv/qdofYz3uhJnpUvpZmFFXlTjgmqb8iUROWCUbEzGKhsneWEKu5r7Ahz5hQ==",
        "userLevel": 145,
        "__v": 0,
        "id": "60a7b122b73b0f39e8323fab"
    }
    ]

GET /users/:userID - mostrar un determinado usuario. Se requiere la cabecera Authorization.


GET /api  - información sobre el uso del API.


GET /api/teams - listar los equipos cargados, se pueden pasar los parametros limit y page para paginar, /api/teams?limit=2&page=2. Se requiere la cabecera Authorization.
Salida esperada:

    [
        {
            "_id": "60a5fee2b88ddc45f7bf7230",
            "id": 143,
            "nombre": "Audax Italiano",
            "sigla": "AUD",
            "paisId": 6,
            "paisNombre": "Chile",
            "tipo": "club",
            ...
        }
    ]



GET /api/teams/:teamId/players - para listar los jugadores por equipo. Se requiere la cabecera Authorization.

GET /api/teams/players/:position para optener un listado de jugadores segun la posición de juego. Se requiere la cabecera Authorization.


# TO DO
Cosas que fuese gustado hacer.

- implementar api log
- centralizar los errores de cabecera.
- agregar test


 


