import crypto from "crypto";

import { pagination } from '../../libs/pagination';
import * as UserModel from "../models/user.model";


export const insert = (req, res) => {
    try {
        let salt = crypto.randomBytes(16).toString('base64');
        let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");

        req.body.password = salt + "$" + hash;
        req.body.userLevel = 145;

        UserModel.createUser(req.body)
            .then((result) => {
                res.status(201).send({id: result._id});
            });   
    } catch (error) {
        res.status(400).send({error: "Bad request"})
        
    }
};

export const list = (req, res) => {
    pagination(req, res, UserModel);
};

export const getById = (req, res) => {
    UserModel.findById(req.params.userId)
        .then((result) => {
            res.status(200).send(result);
        }).catch((error) => {
            console.error("[ERROR] - list teams controller - ",error);
            res.status(404).send();
          });
};