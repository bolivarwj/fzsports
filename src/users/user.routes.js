import { Router } from "express";

import * as UsersController from "./controllers/user.controller";
import * as levelMiddleware from "../authorization/middlewares/auth.level.middleware";
import * as ValidationMiddleware from "../authorization/middlewares/auth.validation.middleware";
import config from "../config";
    

const router = Router();

router.post('/', [
    UsersController.insert
]);

router.get('/', [
    ValidationMiddleware.validJWTNeeded,
    levelMiddleware.minimumLevelRequired(config.NORMAL_LEVEL),
    UsersController.list
]);

router.get('/:userId', [
    ValidationMiddleware.validJWTNeeded,
    levelMiddleware.minimumLevelRequired(config.NORMAL_LEVEL),
    UsersController.getById
]);

export default router;