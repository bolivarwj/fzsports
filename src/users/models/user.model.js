import pkg from 'mongoose';
const { Schema, model } = pkg;


const userSchema = new Schema({
    email: String,
    password: String,
    userLevel: Number
});

userSchema.set('toJSON', {
    virtuals: true
});

userSchema.findById = (key) => {
    return this.model('Users').find({id: this.id}, key);
};

const User = model('Users', userSchema);

export const createUser = (userData) => {
    const user = new User(userData);
    return user.save();
};

export const findByEmail = (email) => {
    return User.find({email: email});
};

export const findById = (id) => {
    return User.findById(id)
        .then((result) => {
            return result.toJSON();;
        });
};

export const list = (perPage, page) => {
    return new Promise((resolve, reject) => {
        User.find()
            .limit(perPage)
            .skip(perPage * page)
            .exec((error, users) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(users);
                }
            })
    });
};