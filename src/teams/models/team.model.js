import pkg from 'mongoose';
const { Schema, model } = pkg;

const teamSchema = new Schema({
    id: Number,
    nombre: String,
    sigla: String,
    paisId: Number,
    paisNombre: String,
    tipo: String,
    jugadores: Object

}, {
    timestamps: true,
    versionKey: false,
    collection: "equipo"
  });

teamSchema.findById = (key) => {
    return this.model('Equipo').find({id: this.id}, key);
};

teamSchema.set('toJSON', {
    virtuals: true
});

const TeamModel = model('Equipo', teamSchema);

export const list = (perPage, page) => {
    return new Promise((resolve, reject) => {
        TeamModel.find()
            .limit(perPage)
            .skip(perPage * page)
            .exec( (error, teams) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(teams);
                }
            })
    });
};

export const playersByTeamId = (id) => {
    return TeamModel.findOne({id: id*1})
        .then((result) => {
            return result.jugadores;
        });
};

export const playersByPosition = (position) => {
    return TeamModel.find({"jugadores.jugador.rol._": position}, {"jugadores.jugador.$": 1});
};