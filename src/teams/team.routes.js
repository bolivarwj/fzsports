import { Router } from "express";

import * as TeamController from "./controllers/team.controller";
import * as levelMiddleware from "../authorization/middlewares/auth.level.middleware";
import * as ValidationMiddleware from "../authorization/middlewares/auth.validation.middleware";
import config from "../config";


const router = Router();

router.get("/", [
    ValidationMiddleware.validJWTNeeded,
    levelMiddleware.minimumLevelRequired(config.NORMAL_LEVEL),
    TeamController.list
]);

router.get("/:idTeam/players", [
    ValidationMiddleware.validJWTNeeded,
    levelMiddleware.minimumLevelRequired(config.NORMAL_LEVEL),
    TeamController.listPlayersByTeamId
]);

router.get("/players/:position", [
    ValidationMiddleware.validJWTNeeded,
    levelMiddleware.minimumLevelRequired(config.NORMAL_LEVEL),
    TeamController.listPlayersByPosition
]);

export default router;
  
