import { pagination } from "../../libs/pagination";
import * as  TeamModel from "../models/team.model";
import config from "../../config";

export const list = (req, res) => {
    pagination(req, res, TeamModel);
};

export const listPlayersByTeamId = (req, res) => {
    TeamModel.playersByTeamId(req.params.idTeam)
        .then((result) => {
            res.status(200).send(result);
        })
        .catch((error) => {
            console.error("[ERROR] - listPlayersByTeamId controller - ",error);
            res.status(400).send({ error: config.GENERIC_ERROR});
          });
};

export const listPlayersByPosition = (req, res) => {
    TeamModel.playersByPosition(req.params.position)
        .then((result) => {
            res.status(200).send(result);
        })
        .catch((error) => {
            console.error("[ERROR] - listPlayersByPosition controller - ",error);
            res.status(400).send({ error: config.GENERIC_ERROR});
          });
};

