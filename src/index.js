import app from "./app";
import "./libs/database";
import config from "./config";
import loadFromFile from "./libs/loadData";

if(config.LOAD_DATA) {
    loadFromFile();
}

app.listen(app.get("port"), () => {
    console.log(`[INFO] Server is running in http://0.0.0.0:${app.get("port")}`);
});