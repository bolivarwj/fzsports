import request from "request";
import xml2js from "xml2js";
import pkg from 'mongoose';
import config from "../config";


const { connection} = pkg;

const insert = (collection, value) => {
    if (Array.isArray(value)){
        for (let i = 0; i < value.length; ++i) {
            insert(collection, value[i]);
        }
    }else{
        if (typeof(value) === "object"){
            collection.insertMany([value], (error, record) =>{
                if (error){
                    throw error;
                } 
                console.log("[INFO] - Load Data Module - data saved in collection ", collection.name);
             });

        }
    }
};

const loadFromFile = () => {
    request.get(config.URL_FILE, (error, response, body) => {
        if (!error && response.statusCode == 200) {
           
            xml2js.parseString(body, { 
                mergeAttrs: true,
                explicitArray: false,
                attrValueProcessors: [(value, name) => {
                    const number = value * 1;
                    if (number){
                        return number
                    }
                    return value
                }]
    
                
             },(err, result) => {
                if(err) {
                    throw err;
                }
    
                for (const [name, object] of Object.entries(result)){
                    Object.entries(object).forEach(([key, value]) => {
                        let collection = connection.collection(key);
                        insert(collection, value);
                    });
    
                }       
        
            });
        }
    });
};

export default loadFromFile;