import mongoose from "mongoose";
import config from "../config";


const options = { 
    poolSize: 10, 
    bufferMaxEntries: 0,
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
    
};

let retry;

const connectWithRetry = () => {
    mongoose.connect(
        config.MONGODB_URI,
        options)
    .then(()=>{
        console.log('[INFO] - mongodb connection module -  connected ', config.MONGODB_URI)
    }).catch((error)=>{
        console.log('[DEBUG] - mongodb connection module - connection unsuccessful, retry in 5 seconds. ', ++retry);
        setTimeout(connectWithRetry, 5000)
    })
};

connectWithRetry();

