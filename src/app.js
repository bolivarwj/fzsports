import express from "express";
import morgan from "morgan";

import config from "./config";
import defauld from "./routes/default";
import TeamRoutes from "./teams/team.routes";
import UserRoutes from "./users/user.routes";
import AuthorizationRoutes from "./authorization/authorization.routes";


const app = express();
app.set("port", config.PORT);
app.use(morgan(config.ENVIROMENT));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use((req, resp, next) => {
    resp.header('Access-Control-Allow-Origin', '*');
    resp.header('Access-Control-Allow-Credentials', 'true');
    resp.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
    resp.header('Access-Control-Expose-Headers', 'Content-Length');
    resp.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range');
    if (req.method === 'OPTIONS') {
        return resp.sendStatus(200);
    } else {
        return next();
    }
});

app.use("/api", defauld);
app.use("/api/teams", TeamRoutes);
app.use("/api/users", UserRoutes);
app.use("/api/auth", AuthorizationRoutes);

app.get('*', (req, res, next) => {
    res.status(301).redirect('/api');
});

export default app;