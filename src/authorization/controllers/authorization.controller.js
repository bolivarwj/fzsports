import crypto from "crypto";
import jwt from "jsonwebtoken";

import config from "../../config";


export const login = (req, res) => {

    try{
        let idRefreshUser = req.body.userId + config.JWT_SECRET;
        let salt = crypto.randomBytes(16).toString('base64');
        let hash = crypto.createHmac('sha512', salt).update(idRefreshUser).digest("base64");

        req.body.refreshKey = salt;
        let token = jwt.sign(req.body, config.JWT_SECRET);
        let buffer = Buffer.from(hash);
        let refreshToken = buffer.toString('base64');
        res.status(201).send({accessToken: token, refreshToken: refreshToken});
    }
    catch (error){
        res.status(400).send({error: "Login token failed"});
    }

};

export const refreshToken = (req, res) => {
    try {
        req.body = req.jwt;
        let token = jwt.sign(req.body, config.JWT_SECRET);
        res.status(201).send({id: token});
    } catch (err) {
        res.status(400).send({errors: "Refresh token faild"});
    }
};