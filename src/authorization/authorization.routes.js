import { Router } from "express";
import * as AuthorizationUserMiddleware from "./middlewares/authorization.user.middleware";
import * as AuthorizationController from "./controllers/authorization.controller";
import * as AuthValidationMiddleware from "./middlewares/auth.validation.middleware";


const router = Router();

router.post('/', [
    AuthorizationUserMiddleware.hasAuthValidFields,
    AuthorizationUserMiddleware.isPasswordAndUserMatch,
    AuthorizationController.login
]);

router.post('/refresh', [
    AuthValidationMiddleware.validJWTNeeded,
    AuthValidationMiddleware.verifyRefreshBodyField,
    AuthValidationMiddleware.validRefreshNeeded,
    AuthorizationController.login
]);

export default router;