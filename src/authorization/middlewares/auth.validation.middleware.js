import jwt from "jsonwebtoken";
import crypto from "crypto";
import config from "../../config";


export const verifyRefreshBodyField = (req, res, next) => {
    if (req.body && req.body.refresh_token) {
        return next();
    } else {
        return res.status(400).send({error: 'It need refresh_token field'});
    }
};

export const validRefreshNeeded = (req, res, next) => {
    let buffer = Buffer.from(req.body.refresh_token, 'base64');
    let refreshToken = buffer.toString();
    let hash = crypto.createHmac('sha512', req.jwt.refreshKey).update(req.jwt.userId + config.JWT_SECRET).digest("base64");
    if (hash === refreshToken) {
        req.body = req.jwt;
        return next();
    } else {
        return res.status(400).send({error: 'Invalid refresh token'});
    }
};

export const validJWTNeeded = (req, res, next) => {

    if (req.headers['authorization']) {
        try {
            let authorization = req.headers['authorization'].split(' ');
            if (authorization[0] === 'Bearer') {
                req.jwt = jwt.verify(authorization[1], config.JWT_SECRET);
                return next();
            }
        } catch (err) {
            return res.status(403).send({error: "Requested resource is forbidden"});
        }
    }
    return res.status(401).send({error: "Unauthorized"});
};