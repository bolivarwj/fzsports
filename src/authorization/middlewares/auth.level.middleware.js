export const minimumLevelRequired = (requiredLevel) => {
    return (req, res, next) => {
        let userLevel = parseInt(req.jwt.userLevel);
        if (userLevel === requiredLevel) {
            return next();
        } else {
            return res.status(403).send("Forbidden");
        }
    };
};