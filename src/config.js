import { config } from "dotenv";

config();

export default {
    PORT: process.env.PORT || 9000,
    MONGODB_URI : process.env.MONGODB_URI || "mongodb://localhost:27017/fzsports",
    URL_FILE: process.env.URL_FILE || "https://fx-nunchee-assets.s3.amazonaws.com/data/sports.xml",
    SECRET:  process.env.SECRET || "fzsports",
    ENVIROMENT:  process.env.ENVIROMENT || "dev",
    GENERIC_ERROR: "Something went wrong",
    JWT_SECRET:  process.env.JWT_SECRET || "fzsportsGeate!!",
    JWT_EXPIRATION:  process.env.JWT_EXPIRATION || 36000,
    NORMAL_LEVEL: 145,
    FINAL_LEVEL: 11,
    ADMIN_LEVEL: 2121,
    LOAD_DATA:  process.env.LOAD_DATA || true
}