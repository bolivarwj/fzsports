
FROM node:15-alpine

RUN mkdir -p /usr/app

WORKDIR /usr/app

COPY . .

RUN yarn install

EXPOSE 9000

CMD ["yarn", "run", "start"]