const benchmark =  require('benchmark');
const _ = require('lodash');

const suite = new benchmark.Suite

let array = [];

for (let i = 0; i < 1000; i++) {
  array.push(i);
}

suite
.add('while', () => {
    let i = 0;
    while (i < array.length) {
        const item = array[i];
        const foobar = item * 2;
        i++;
    }
})
.add('for (let i)', () => {
  for (let i = 0; i < array.length; ++i) {
    const item = array[i];

    const foobar = item * 2;
  }
})
.add('for (let of)', () => {
  for (let item of array) {
    const foobar = item * 2;
  }
})
.add('for (let in)', () => {
    for (let item in array) {
      const foobar = item * 2;
    }
  })
.add('foreach', () => {
  array.forEach(item => {
    const foobar = item * 2;
  });
})
.add('_.forEach', () => {
    _.forEach(array, (item) => {
        const foobar = item * 2;
      });
})
.on('cycle', (event) => {
  console.log(String(event.target));
})
.on('complete', () => {
  console.log('Fastest is ' + this.filter('fastest').map('name'));
})
.run();