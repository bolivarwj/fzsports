import request from "request";
import xml2js from "xml2js";
import mongoose from "mongoose";

mongoose.connect( process.env.MONGODB_URI || "mongodb://localhost:27017/fzsports", {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;

const FILE = "https://fx-nunchee-assets.s3.amazonaws.com/data/sports.xml";


const insert = (collection, value) => {
    if (Array.isArray(value)){
        for (let i = 0; i < value.length; ++i) {
            insert(collection, value[i]);
        }
    }else{
        if (typeof(value) === "object"){
            collection.insertMany([value], (error, record) =>{
                if (error){
                    throw error;
                } 
                console.log("data saved");
             });

        }
    }
};

request.get(FILE, (error, response, body) => {
    if (!error && response.statusCode == 200) {
       
        xml2js.parseString(body, { 
            mergeAttrs: true,
            explicitArray: false,
            attrValueProcessors: [(value, name) => {
                const number = value * 1;
                if (number){
                    return number
                }
                return value
            }]

            
         },(err, result) => {
            if(err) {
                throw err;
            }

            for (const [name, object] of Object.entries(result)){
                Object.entries(object).forEach(([key, value]) => {
                    let collection = db.collection(key);
                    insert(collection, value);
                });

            }       
    
        });
        console.log("Load data completed");
        process.exit();
    }
});